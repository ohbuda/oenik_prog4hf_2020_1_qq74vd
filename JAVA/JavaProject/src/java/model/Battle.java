/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Random;

/**
 *
 * @author USERPC
 */
public class Battle {

    static Random r = new Random();
    private String Name;
    private String Species;
    private String Gender;
    private int Dob;
    private int Dod;
    
    public Battle() {
        Name = RandomString(r.nextInt(30));
        Species = RandomString(r.nextInt(30));
        Gender = RandomString(r.nextInt(30));
        Dod = r.nextInt(5000);
        Dob = r.nextInt(Dod);
    }

    public String getName() {
        return Name;
    }

    public String getSpecies() {
        return Species;
    }

    public String getGender() {
        return Gender;
    }

    public int getDob() {
        return Dob;
    }

    public int getDod() {
        return Dod;
    }
    
    
    static String RandomString(int n){
        String allChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                                    + "0123456789"
                                    + "abcdefghijklmnopqrstuvxyz";
        String result = "";
        
        for(int i = 0; i<n;i++){
            result+=allChars.charAt(r.nextInt(allChars.length()));
        }
        return result;
}
    
}
