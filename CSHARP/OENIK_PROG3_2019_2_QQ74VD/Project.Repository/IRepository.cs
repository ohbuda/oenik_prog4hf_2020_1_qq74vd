﻿// <copyright file="IRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Project.Repository
{
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Interface for a generic repository.
    /// </summary>
    /// <typeparam name="TEntity">Generic type.</typeparam>
    public interface IRepository<TEntity>
        where TEntity : class
    {
        /// <summary>
        /// Creates an element.
        /// </summary>
        /// <param name="t">Generic Type.</param>
        void Create(TEntity t);

        /// <summary>
        /// Gets element with specific ID.
        /// </summary>
        /// <param name="id">ID of an element.</param>
        /// <returns>One element.</returns>
        TEntity GetById(int id);

        /// <summary>
        /// Gets all elements.
        /// </summary>
        /// <returns>Every element in the IEnum.</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Justification = "Changing it to a property would'nt work")]
        IQueryable<TEntity> GetAll();

        /// <summary>
        /// Deletes an element.
        /// </summary>
        /// <param name="t">Generic type.</param>
        void Delete(TEntity t);
    }
}
