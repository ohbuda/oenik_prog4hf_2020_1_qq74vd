﻿// <copyright file="ICharacterRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Project.Repository
{
    using Project.Data;
    using System;

    /// <summary>
    /// Repository interface for characters.
    /// </summary>
    public interface ICharacterRepository : IRepository<Character>
    {
        /// <summary>
        /// Changes Name field of one entry in the Characters table.
        /// </summary>
        /// <param name="id">ID of object.</param>
        /// <param name="newName">What the Name will be changed to.</param>
        void UpdateName(int id, string newName);
        void Update(int id, string name, string gender, string species, DateTime dob, DateTime dod);
    }
}
