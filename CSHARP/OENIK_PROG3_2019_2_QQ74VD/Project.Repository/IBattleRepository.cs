﻿// <copyright file="IBattleRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Project.Repository
{
    using Project.Data;

    /// <summary>
    /// Repository interface for battles.
    /// </summary>
    public interface IBattleRepository : IRepository<Battle>
    {
        /// <summary>
        /// Changes outcome field of one entry in the Battle table.
        /// </summary>
        /// <param name="id">ID of object.</param>
        /// <param name="newOutcome">What the outcome will be changed to.</param>
        void UpdateOutcome(int id, string newOutcome);
    }
}
