﻿// <copyright file="Repository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Project.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Project.Data;

    /// <summary>
    /// Implementation of generic Repository.
    /// </summary>
    /// <typeparam name="TEntity">Generic type.</typeparam>
    public abstract class Repository<TEntity> : IRepository<TEntity>
        where TEntity : class
    {
        /// <summary>
        /// Database context to be used to access the database.
        /// </summary>
        private DbContext dbContext;

        /// <summary>
        /// Initializes a new instance of the <see cref="Repository{T}"/> class.
        /// </summary>
        /// <param name="dbContext">Database context.</param>
        protected Repository(DbContext dbContext)
        {
            this.DbContext = dbContext;
        }

        /// <summary>
        /// Gets or sets and gets database context.
        /// </summary>
        protected DbContext DbContext { get => this.dbContext; set => this.dbContext = value; }

        /// <inheritdoc/>
        public abstract void Create(TEntity t);

        /// <inheritdoc/>
        public abstract void Delete(TEntity t);

        /// <inheritdoc/>
        public abstract IQueryable<TEntity> GetAll();

        /// <inheritdoc/>
        public abstract TEntity GetById(int id);
    }
}
