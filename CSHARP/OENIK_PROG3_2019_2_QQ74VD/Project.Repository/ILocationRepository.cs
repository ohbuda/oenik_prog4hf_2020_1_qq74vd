﻿// <copyright file="ILocationRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Project.Repository
{
    using Project.Data;

    /// <summary>
    /// Repository interface for locations.
    /// </summary>
    public interface ILocationRepository : IRepository<Location>
    {
        /// <summary>
        /// Changes Category field of one entry in the Locations table.
        /// </summary>
        /// <param name="id">ID of object.</param>
        /// <param name="newCategory">What the Category will be changed to.</param>
        void UpdateCategory(int id, string newCategory);
    }
}
