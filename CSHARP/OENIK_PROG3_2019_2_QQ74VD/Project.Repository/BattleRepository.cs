﻿// <copyright file="BattleRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Project.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Project.Data;

    /// <summary>
    /// Repository for Battle table.
    /// </summary>
    public class BattleRepository : Repository<Battle>, IBattleRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BattleRepository"/> class.
        /// </summary>
        /// <param name="dbContext">Database context.</param>
        public BattleRepository(DbContext dbContext)
            : base(dbContext)
        {
        }

        /// <summary>
        /// Adds a new object to the table. Throws error if ID already exists.
        /// </summary>
        /// <param name="t">Battle object.</param>
        public override void Create(Battle t)
        {
            if (this.DbContext.Set<Battle>().Where(x => x.Id == t.Id).Any())
            {
                throw new ArgumentException("ID already in table.");
            }

            this.DbContext.Set<Project.Data.Battle>().Add(t);
            this.DbContext.SaveChanges();
        }

        /// <inheritdoc/>
        public override void Delete(Battle t)
        {
            try
            {
                this.GetById(t.Id);
                var conn1_del = this.DbContext.Set<Project.Data.Con_CharToBatt>()
                    .Where(x => x.Batt_Id == t.Id);
                var conn2_del = this.DbContext.Set<Project.Data.Con_BattToLoc>()
                    .Where(x => x.Batt_Id == t.Id);
                foreach (Con_CharToBatt con_CharToBatt in conn1_del)
                {
                    this.DbContext.Set<Con_CharToBatt>().Remove(con_CharToBatt);
                }

                foreach (Con_BattToLoc con_BattToLoc in conn2_del)
                {
                    this.DbContext.Set<Con_BattToLoc>().Remove(con_BattToLoc);
                }

                this.DbContext.Set<Project.Data.Battle>().Remove(t);
                this.DbContext.SaveChanges();
            }
            catch (Exception)
            {
                throw new FormatException("ID doesn't exist.");
            }
        }

        /// <inheritdoc/>
        public override IQueryable<Battle> GetAll()
        {
            return this.DbContext.Set<Project.Data.Battle>();
        }

        /// <inheritdoc/>
        public override Battle GetById(int id)
        {
            return this.DbContext.Set<Project.Data.Battle>().Where(x => x.Id == id).FirstOrDefault();
        }

        /// <inheritdoc/>
        public void UpdateOutcome(int id, string newOutcome)
        {
                this.DbContext.Set<Project.Data.Battle>().Where(x => x.Id == id).FirstOrDefault().Outcome = newOutcome;
                this.DbContext.SaveChanges();
        }
    }
}
