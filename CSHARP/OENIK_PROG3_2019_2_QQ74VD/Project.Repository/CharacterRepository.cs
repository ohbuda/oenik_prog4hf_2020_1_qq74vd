﻿// <copyright file="CharacterRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Project.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using Project.Data;

    /// <summary>
    /// Repository implementation of characters.
    /// </summary>
    public class CharacterRepository : Repository<Character>, ICharacterRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CharacterRepository"/> class.
        /// </summary>
        /// <param name="dbContext">Database context.</param>
        public CharacterRepository(DbContext dbContext)
            : base(dbContext)
        {
        }

        /// <inheritdoc/>
        public override void Create(Character t)
        {
            if (t.Id == 0)
            {
                for (int i = 0; i < GetAll().Count(); i++)
                {
                    if (i == 0)
                    {
                        if (GetAll().First().Id != 1)
                        {
                            t.Id = 1;
                            break;
                        }
                    }
                    else
                    {
                        if (GetAll().ToList()[i].Id - 1 != GetAll().ToList()[i - 1].Id)
                        {
                            t.Id = i + 1;
                            break;
                        }
                    }
                }
                if (t.Id == 0)
                {
                    t.Id = GetAll().Count() + 1;
                }
            }
            this.DbContext.Set<Project.Data.Character>().Add(t);
            this.DbContext.SaveChanges();
        }

        /// <inheritdoc/>
        public override void Delete(Character t)
        {
            try
            {
                this.GetById(t.Id);
                var conn1_del = this.DbContext.Set<Project.Data.Con_CharToBatt>()
                    .Where(x => x.Char_Id == t.Id);
                var conn2_del = this.DbContext.Set<Project.Data.Con_CharToChar>()
                    .Where(x => x.Char_Id == t.Id || x.Char2_Id == t.Id);
                foreach (Con_CharToBatt con_CharToBatt in conn1_del)
                {
                    this.DbContext.Set<Con_CharToBatt>().Remove(con_CharToBatt);
                }

                foreach (Con_CharToChar con_CharToChar in conn2_del)
                {
                    this.DbContext.Set<Con_CharToChar>().Remove(con_CharToChar);
                }

                this.DbContext.Set<Project.Data.Character>().Remove(t);
                this.DbContext.SaveChanges();
            }
            catch (Exception)
            {
                throw new FormatException("ID doesn't exist.");
            }
        }

        /// <inheritdoc/>
        public override IQueryable<Character> GetAll()
        {
            return this.DbContext.Set<Project.Data.Character>();
        }

        /// <inheritdoc/>
        public override Character GetById(int id)
        {
            return this.DbContext.Set<Project.Data.Character>().Where(x => x.Id == id).FirstOrDefault();
        }

        /// <inheritdoc/>
        public void UpdateName(int id, string newName)
        {
            this.DbContext.Set<Project.Data.Character>().Where(x => x.Id == id).FirstOrDefault().Name = newName;
        }

        public void Update(int id, string name,string gender, string species, DateTime dob, DateTime dod)
        {
            this.DbContext.Set<Project.Data.Character>().Where(x => x.Id == id).FirstOrDefault().Name = name;
            this.DbContext.Set<Project.Data.Character>().Where(x => x.Id == id).FirstOrDefault().Gender = gender;
            this.DbContext.Set<Project.Data.Character>().Where(x => x.Id == id).FirstOrDefault().Species = species;
            this.DbContext.Set<Project.Data.Character>().Where(x => x.Id == id).FirstOrDefault().Dob = dob;
            this.DbContext.Set<Project.Data.Character>().Where(x => x.Id == id).FirstOrDefault().Dod = dod;

        }
    }
}
