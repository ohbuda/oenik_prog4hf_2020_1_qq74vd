﻿// <copyright file="LocationRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Project.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using Project.Data;

    /// <summary>
    /// Repository implementation of Locations.
    /// </summary>
    public class LocationRepository : Repository<Location>, ILocationRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LocationRepository"/> class.
        /// </summary>
        /// <param name="dbContext">Context of database.</param>
        public LocationRepository(DbContext dbContext)
            : base(dbContext)
        {
        }

        /// <inheritdoc/>
        public override void Create(Location t)
        {
            if (this.DbContext.Set<Location>().Where(x => x.Id == t.Id).Any())
            {
                throw new ArgumentException("ID already in table.");
            }

            this.DbContext.Set<Project.Data.Location>().Add(t);
            this.DbContext.SaveChanges();
        }

        /// <inheritdoc/>
        public override void Delete(Location t)
        {
            try
            {
                this.GetById(t.Id);
                var conn_del = this.DbContext.Set<Project.Data.Con_BattToLoc>()
                    .Where(x => x.Loc_Id == t.Id);

                foreach (Con_BattToLoc con_BattToLoc in conn_del)
                {
                    this.DbContext.Set<Con_BattToLoc>().Remove(con_BattToLoc);
                }

                this.DbContext.Set<Project.Data.Location>().Remove(t);
                this.DbContext.SaveChanges();
            }
            catch (Exception)
            {
                throw new FormatException("ID doesn't exist.");
            }
        }

        /// <inheritdoc/>
        public override IQueryable<Location> GetAll()
        {
            return this.DbContext.Set<Project.Data.Location>();
        }

        /// <inheritdoc/>
        public override Location GetById(int id)
        {
            return this.DbContext.Set<Project.Data.Location>().Where(x => x.Id == id).FirstOrDefault();
        }

        /// <inheritdoc/>
        public void UpdateCategory(int id, string newCategory)
        {
            this.DbContext.Set<Project.Data.Location>().Where(x => x.Id == id).FirstOrDefault().Category = newCategory;
        }
    }
}
