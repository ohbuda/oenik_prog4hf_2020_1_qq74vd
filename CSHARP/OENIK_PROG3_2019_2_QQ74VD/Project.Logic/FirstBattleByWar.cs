﻿// <copyright file="FirstBattleByWar.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Project.Logic
{
    using System;

    /// <summary>
    /// Class made for a Non-Crud operation.
    /// </summary>
    public class FirstBattleByWar
    {
        /// <summary>
        /// Gets or sets the name of a war.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the date of the first battle in a war.
        /// </summary>
        public DateTime FirstWarDate { get; set; }

        /// <inheritdoc/>
        public override string ToString()
        {
            return $"Name={this.Name}, Date of first war={this.FirstWarDate}";
        }

        /// <inheritdoc/>
        public override bool Equals(object obj)
        {
            if (obj is FirstBattleByWar)
            {
                FirstBattleByWar other = obj as FirstBattleByWar;
                return this.Name == other.Name && this.FirstWarDate == other.FirstWarDate;
            }

            return false;
        }

        /// <inheritdoc/>
        public override int GetHashCode()
        {
            return this.Name.GetHashCode() + this.FirstWarDate.GetHashCode();
        }
    }
}
