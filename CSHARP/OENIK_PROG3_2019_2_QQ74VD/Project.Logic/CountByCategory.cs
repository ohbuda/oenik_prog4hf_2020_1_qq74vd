﻿// <copyright file="CountByCategory.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Project.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Class made for a Non-Crud operation.
    /// </summary>
    public class CountByCategory
    {
        /// <summary>
        /// Gets or sets a category.
        /// </summary>
        public string Category { get; set; }

        /// <summary>
        /// Gets or sets the count of the categories.
        /// </summary>
        public int Count { get; set; }

        /// <inheritdoc/>
        public override string ToString()
        {
            return $"Category={this.Category}, Count={this.Count}";
        }

        /// <inheritdoc/>
        public override bool Equals(object obj)
        {
            if (obj is CountByCategory)
            {
                CountByCategory other = obj as CountByCategory;
                return this.Category == other.Category && this.Count == other.Count;
            }

            return false;
        }

        /// <inheritdoc/>
        public override int GetHashCode()
        {
            return this.Category.GetHashCode() + this.Count.GetHashCode();
        }
    }
}
