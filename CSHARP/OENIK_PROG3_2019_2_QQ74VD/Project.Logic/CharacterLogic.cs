﻿// <copyright file="CharacterLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Project.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Project.Data;
    using Project.Repository;

    /// <summary>
    /// Implementations of the logic functions for the Character table.
    /// </summary>
    public class CharacterLogic : ICharacterLogic
    {
        private ICharacterRepository repository;

        /// <summary>
        /// Initializes a new instance of the <see cref="CharacterLogic"/> class.
        /// </summary>
        /// <param name="repository">Repo of Characters.</param>
        public CharacterLogic(ICharacterRepository repository)
        {
            this.repository = repository;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CharacterLogic"/> class. Factory.
        /// </summary>
        public CharacterLogic()
        {
            this.repository = new CharacterRepository(new DatabaseEntities7());
        }

        public void Add(Character t)
        {
            this.repository.Create(t);
        }

        /// <inheritdoc/>
        public bool AddConfirm(Character t)
        {
            int noOfChars = repository.GetAll().Count();
            this.repository.Create(t);
            if (repository.GetAll().Count() - 1 == noOfChars)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <inheritdoc/>
        public void Delete(Character t)
        {
            this.repository.Delete(t);
        }

        public bool Delete(int id)
        {
            Character t = repository.GetById(id);
            if(t != null)
            {
                this.repository.Delete(t);
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <inheritdoc/>
        public IEnumerable<Character> GetAll()
        {
            return this.repository.GetAll();
        }

        /// <inheritdoc/>
        public Character GetById(int id)
        {
            return this.repository.GetById(id);
        }

        /// <inheritdoc/>
        public bool UpdateName(int id, string newName)
        {
            if (GetById(id) != null)
            {
                this.repository.UpdateName(id, newName);
                return true;
            }
            return false;
        }

        public bool Update(int id, string name, string gender, string species, DateTime dob, DateTime dod)
        {
            if(GetById(id) != null)
            {
                Delete(id);
                Add(new Data.Character()
                {
                    Name = name,
                    Species = species,
                    Dob = dob,
                    Dod = dod,
                    Gender = gender,
                });
                return true;
            }
            return false;
        }

        /// <inheritdoc/>
        public List<CountGender> CountByGender()
        {
            var q = from character in this.repository.GetAll()
                    group character by character.Gender into grp
                    select new CountGender { Gender = grp.Key, Count = grp.Count() };
            return q.ToList();
        }

        /// <inheritdoc/>
        public List<Character> SearchForSpecies(string s)
        {
            var q = from character in this.repository.GetAll()
                    where character.Species == s
                    select character;
            return q.ToList();
        }

        void ICharacterLogic.UpdateName(int id, string newName)
        {
            throw new NotImplementedException();
        }
    }
}
