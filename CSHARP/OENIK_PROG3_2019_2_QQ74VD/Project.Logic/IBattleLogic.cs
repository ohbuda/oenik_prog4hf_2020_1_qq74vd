﻿// <copyright file="IBattleLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Project.Logic
{
    using System.Collections.Generic;
    using Project.Data;

    /// <summary>
    /// Interface logic for Battle table.
    /// </summary>
    internal interface IBattleLogic : ILogic<Battle>
    {
        /// <summary>
        /// Non-CRUD operation. Groups by wars and also outputs the date of the first known battle.
        /// </summary>
        /// <returns>Returns list of wars and the date of the first known battle.</returns>
        IList<FirstBattleByWar> FirstBattleByWar();

        /// <summary>
        /// Calls UpdateOutcome of repo.
        /// </summary>
        /// <param name="id">Id of wanted object.</param>
        /// <param name="newOutcome">New wanted outcome.</param>
        void UpdateOutcome(int id, string newOutcome);
    }
}
