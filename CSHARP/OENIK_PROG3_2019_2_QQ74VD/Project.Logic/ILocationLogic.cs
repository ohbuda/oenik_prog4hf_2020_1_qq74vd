﻿// <copyright file="ILocationLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Project.Logic
{
    using System.Collections.Generic;
    using Project.Data;

    /// <summary>
    /// Interface logic for Location table.
    /// </summary>
    internal interface ILocationLogic : ILogic<Location>
    {
        /// <summary>
        /// Calls the Update Category of the repo.
        /// </summary>
        /// <param name="id">Id of object.</param>
        /// <param name="newCategory">New category string.</param>
        void UpdateCategory(int id, string newCategory);

        /// <summary>
        /// Groups the table into categories then counts how many non-destoryed location there are
        /// in a location.
        /// </summary>
        /// <returns>List of categories and counts.</returns>
        List<CountByCategory> CountingByCategory();
    }
}
