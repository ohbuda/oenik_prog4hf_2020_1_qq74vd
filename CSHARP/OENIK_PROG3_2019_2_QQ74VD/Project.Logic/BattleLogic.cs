﻿// <copyright file="BattleLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Project.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Project.Data;
    using Project.Repository;

    /// <summary>
    /// Implementations of the logic functions for the Battles table.
    /// </summary>
    public class BattleLogic : IBattleLogic
    {
        private IBattleRepository repository;

        /// <summary>
        /// Initializes a new instance of the <see cref="BattleLogic"/> class.
        /// </summary>
        /// <param name="repository">Repo of Battles.</param>
        public BattleLogic(IBattleRepository repository)
        {
            this.repository = repository;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BattleLogic"/> class. Factory.
        /// </summary>
        public BattleLogic()
        {
            this.repository = new BattleRepository(new DatabaseEntities7());
        }

        /// <inheritdoc/>
        public void Add(Battle t)
        {
            this.repository.Create(t);
        }

        /// <inheritdoc/>
        public void Delete(Battle t)
        {
            this.repository.Delete(t);
        }

        /// <inheritdoc/>
        public IEnumerable<Battle> GetAll()
        {
            return this.repository.GetAll();
        }

        /// <inheritdoc/>
        public Battle GetById(int id)
        {
            return this.repository.GetById(id);
        }

        /// <inheritdoc/>
        public void UpdateOutcome(int id, string newOutcome)
        {
            this.repository.UpdateOutcome(id, newOutcome);
        }

        /// <inheritdoc/>
        public IList<FirstBattleByWar> FirstBattleByWar()
        {
            var q = from battle in this.repository.GetAll()
                    where battle.War != null
                    group battle by battle.War into grp
                    select new FirstBattleByWar
                    {
                        Name = grp.Key,
                        FirstWarDate = grp.Min(x => x.Dos.Value),
                    };
            return q.ToList();
        }
    }
}
