﻿// <copyright file="ILogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace Project.Logic
{
    using System.Collections.Generic;

    /// <summary>
    /// Logic interface.
    /// </summary>
    /// <typeparam name="T">Generic type.</typeparam>
    public interface ILogic<T>
    {
        /// <summary>
        /// Gets one object if it exists.
        /// </summary>
        /// <param name="id">Id of wanted object.</param>
        /// <returns>One object.</returns>
        T GetById(int id);

        /// <summary>
        /// Gets all objects in table.
        /// </summary>
        /// <returns>All object in table.</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Justification = "Changing it to a property wouldn't work")]
        IEnumerable<T> GetAll();

        /// <summary>
        /// Adds object to table.
        /// </summary>
        /// <param name="t">Generic type.</param>
        void Add(T t);

        /// <summary>
        /// Removes object from table if it exists.
        /// </summary>
        /// <param name="t">Generic type.</param>
        void Delete(T t);
    }
}
