﻿// <copyright file="CountGender.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Project.Logic
{
    /// <summary>
    /// Class made for a Non-Crud operation.
    /// </summary>
    public class CountGender
    {
        /// <summary>
        /// Gets or sets a gender.
        /// </summary>
        public string Gender { get; set; }

        /// <summary>
        /// Gets or sets the count of the genders.
        /// </summary>
        public int Count { get; set; }

        /// <inheritdoc/>
        public override string ToString()
        {
            return $"Gender={this.Gender}, Count={this.Count}";
        }

        /// <inheritdoc/>
        public override bool Equals(object obj)
        {
            if (obj is CountGender)
            {
                CountGender other = obj as CountGender;
                return this.Gender == other.Gender && this.Count == other.Count;
            }

            return false;
        }

        /// <inheritdoc/>
        public override int GetHashCode()
        {
            return this.Gender.GetHashCode() + this.Count.GetHashCode();
        }
    }
}
