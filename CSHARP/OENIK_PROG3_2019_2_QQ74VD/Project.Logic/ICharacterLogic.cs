﻿// <copyright file="ICharacterLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Project.Logic
{
    using System.Collections.Generic;
    using Project.Data;

    /// <summary>
    /// Interface logic for Character table.
    /// </summary>
    internal interface ICharacterLogic : ILogic<Character>
    {
        /// <summary>
        /// Calls update name function of repo.
        /// </summary>
        /// <param name="id">Id of object.</param>
        /// <param name="newName">new name of character.</param>
        void UpdateName(int id, string newName);

        /// <summary>
        /// Non-CRUD method. Groups the characters table by gender and counts them.
        /// </summary>
        /// <returns>List of genders and how many of them there are.</returns>
        List<CountGender> CountByGender();

        /// <summary>
        /// Searches the table.
        /// </summary>
        /// <param name="s">Species to look for.</param>
        /// <returns>List of characters with same species as inputted.</returns>
        List<Character> SearchForSpecies(string s);
    }
}
