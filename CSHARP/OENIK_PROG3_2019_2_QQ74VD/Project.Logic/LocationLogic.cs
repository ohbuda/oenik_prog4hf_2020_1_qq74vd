﻿// <copyright file="LocationLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Project.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Project.Data;
    using Project.Repository;

    /// <summary>
    /// Implementations of the logic functions for the Location table.
    /// </summary>
    public class LocationLogic : ILocationLogic
    {
        private ILocationRepository repository;

        /// <summary>
        /// Initializes a new instance of the <see cref="LocationLogic"/> class.
        /// </summary>
        /// <param name="repository">Repo of Locations.</param>
        public LocationLogic(ILocationRepository repository)
        {
            this.repository = repository;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LocationLogic"/> class. Factory.
        /// </summary>
        public LocationLogic()
        {
            this.repository = new LocationRepository(new DatabaseEntities7());
        }

        /// <inheritdoc/>
        public void Add(Location t)
        {
            this.repository.Create(t);
        }

        /// <inheritdoc/>
        public void Delete(Location t)
        {
            this.repository.Delete(t);
        }

        /// <inheritdoc/>
        public IEnumerable<Location> GetAll()
        {
            return this.repository.GetAll();
        }

        /// <inheritdoc/>
        public Location GetById(int id)
        {
            return this.repository.GetById(id);
        }

        /// <inheritdoc/>
        public void UpdateCategory(int id, string newCategory)
        {
            this.repository.UpdateCategory(id, newCategory);
        }

        /// <inheritdoc/>
        public List<CountByCategory> CountingByCategory()
        {
            var q = from location in this.repository.GetAll()
                    where location.Dod != null
                    group location by location.Category into grp
                    select new CountByCategory
                    {
                        Category = grp.Key,
                        Count = grp.Count(),
                    };
            return q.ToList();
        }
    }
}
