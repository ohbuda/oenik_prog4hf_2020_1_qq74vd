﻿// <copyright file="TestClass.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Project.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Moq;
    using NUnit.Framework;
    using Project.Data;
    using Project.Repository;

    /// <summary>
    /// Contains unit tests using a Moq mock database to test if the logic functions work propely.
    /// </summary>
    [TestFixture]
    public class TestClass
    {
        /// <summary>
        /// Adds data to Battles table, assert that it got added.
        /// </summary>
        [Test]
        public void AddingToDatabaseBattles()
        {
            Battle battle = new Battle { Id = 222, Name = "1" };
            Mock<IBattleRepository> repo = new Mock<IBattleRepository>();
            BattleLogic battleLogic = new BattleLogic(repo.Object);
            repo.Setup(x => x.Create(battle));
            battleLogic.Add(battle);

            repo.Verify(x => x.Create(battle), Times.Once());
        }

        /// <summary>
        /// Adds data to Locations table, assert that it got added.
        /// </summary>
        [Test]
        public void AddingToDatabaseLocations()
        {
            Location location = new Location { Id = 222, Name = "1" };
            Mock<ILocationRepository> repo = new Mock<ILocationRepository>();
            LocationLogic locationLogic = new LocationLogic(repo.Object);
            repo.Setup(x => x.Create(location));
            locationLogic.Add(location);

            repo.Verify(x => x.Create(location), Times.Once());
        }

        /// <summary>
        /// Adds data to Characters table, assert that it got added.
        /// </summary>
        [Test]
        public void AddingToDatabaseBattlesCharacters()
        {
            Character character = new Character { Id = 222, Name = "1" };
            Mock<ICharacterRepository> repo = new Mock<ICharacterRepository>();
            CharacterLogic characterLogic = new CharacterLogic(repo.Object);
            repo.Setup(x => x.Create(character));
            characterLogic.Add(character);

            repo.Verify(x => x.Create(character), Times.Once());
        }

        /// <summary>
        /// Adds data to mock database, then deletes it from the Battles table.
        /// </summary>
        [Test]
        public void RemoveFromDatabaseBattles()
        {
            Battle battle = new Battle { Id = 222, Name = "1" };
            Mock<IBattleRepository> repo = new Mock<IBattleRepository>();
            BattleLogic battleLogic = new BattleLogic(repo.Object);
            repo.Setup(x => x.Create(battle));
            battleLogic.Add(battle);
            repo.Setup(x => x.Delete(battle));
            battleLogic.Delete(battle);

            repo.Verify(x => x.Create(battle), Times.Once);
            repo.Verify(x => x.Delete(battle), Times.Once);
        }

        /// <summary>
        /// Adds data to mock database, then deletes itfrom the Locations table.
        /// </summary>
        [Test]
        public void RemoveFromDatabaseLocations()
        {
            Location location = new Location { Id = 222, Name = "1" };
            Mock<ILocationRepository> repo = new Mock<ILocationRepository>();
            LocationLogic locationLogic = new LocationLogic(repo.Object);
            repo.Setup(x => x.Create(location));
            locationLogic.Add(location);
            repo.Setup(x => x.Delete(location));
            locationLogic.Delete(location);

            repo.Verify(x => x.Create(location), Times.Once);
            repo.Verify(x => x.Delete(location), Times.Once);
        }

        /// <summary>
        /// Adds data to mock database, then deletes it from the Characters table.
        /// </summary>
        [Test]
        public void RemoveFromDatabaseCharacters()
        {
            Character character = new Character { Id = 222, Name = "1" };
            Mock<ICharacterRepository> repo = new Mock<ICharacterRepository>();
            CharacterLogic locationLogic = new CharacterLogic(repo.Object);
            repo.Setup(x => x.Create(character));
            locationLogic.Add(character);
            repo.Setup(x => x.Delete(character));
            locationLogic.Delete(character);

            repo.Verify(x => x.Create(character), Times.Once);
            repo.Verify(x => x.Delete(character), Times.Once);
        }

        /// <summary>
        /// Adds data to mock Battles database, then tests the GetAll function.
        /// </summary>
        [Test]
        public void ReadAllBattles()
        {
            List<Battle> battles = new List<Battle>() { new Battle { Id = 222, Name = "1" } };
            Mock<IBattleRepository> repo = new Mock<IBattleRepository>();
            repo.Setup(x => x.GetAll()).Returns(battles.AsQueryable());

            Assert.That(repo.Object.GetAll().Count, Is.EqualTo(battles.Count));
        }

        /// <summary>
        /// Adds data to mock Locations database, then tests the GetAll function.
        /// </summary>
        [Test]
        public void ReadAllLocations()
        {
            List<Location> locations = new List<Location>() { new Location { Id = 222, Name = "1" } };
            Mock<ILocationRepository> repo = new Mock<ILocationRepository>();
            repo.Setup(x => x.GetAll()).Returns(locations.AsQueryable());

            Assert.That(repo.Object.GetAll().Count, Is.EqualTo(locations.Count));
        }

        /// <summary>
        /// Adds data to mock Characters database, then tests the GetAll function.
        /// </summary>
        [Test]
        public void ReadAllCharacters()
        {
            List<Character> characters = new List<Character>() { new Character { Id = 222, Name = "1" } };
            Mock<ICharacterRepository> repo = new Mock<ICharacterRepository>();
            repo.Setup(x => x.GetAll()).Returns(characters.AsQueryable());

            Assert.That(repo.Object.GetAll().Count, Is.EqualTo(characters.Count));
        }

        /// <summary>
        /// Tests the outcome changer function of the Battle table.
        /// </summary>
        [Test]
        public void UpdateOutcome()
        {
            List<Battle> battles = new List<Battle>()
            {
                new Battle { Id = 222, Name = "1", Outcome = "s" },
            };
            Mock<IBattleRepository> repo = new Mock<IBattleRepository>();
            BattleLogic battleLogic = new BattleLogic(repo.Object);

            repo.Setup(x => x.UpdateOutcome(222, "d"));
            battleLogic.UpdateOutcome(222, "d");

            repo.Verify(x => x.UpdateOutcome(222, "d"), Times.Exactly(1));
        }

        /// <summary>
        /// Tests the UpdateName function of the characters table.
        /// </summary>
        [Test]
        public void UpdateName()
        {
            List<Character> characters = new List<Character>()
            {
                new Character { Id = 222, Name = "1", },
            };
            Mock<ICharacterRepository> repo = new Mock<ICharacterRepository>();
            CharacterLogic characterLogic = new CharacterLogic(repo.Object);

            repo.Setup(x => x.UpdateName(222, "d"));
            characterLogic.UpdateName(222, "d");

            repo.Verify(x => x.UpdateName(222, "d"), Times.Exactly(1));
        }

        /// <summary>
        /// Tests the UpdateCategory function of the locations table.
        /// </summary>
        [Test]
        public void UpdateCategory()
        {
            List<Location> locations = new List<Location>()
            {
                new Location { Id = 222, Category = "1", },
            };
            Mock<ILocationRepository> repo = new Mock<ILocationRepository>();
            LocationLogic locationLogic = new LocationLogic(repo.Object);

            repo.Setup(x => x.UpdateCategory(222, "d"));
            locationLogic.UpdateCategory(222, "d");

            repo.Verify(x => x.UpdateCategory(222, "d"), Times.Exactly(1));
        }

        /// <summary>
        /// Tests the FirstBattleByWar Non-CRUD function.
        /// </summary>
        [Test]
        public void TestFirstBattleByWar()
        {
            Mock<IBattleRepository> repo = new Mock<IBattleRepository>();
            List<Battle> battles = new List<Battle>()
            {
                new Battle() { Dos = DateTime.Parse("1/1111"), War = "a" },
                new Battle() { Dos = DateTime.Parse("1/1112"), War = "a" },
                new Battle() { Dos = DateTime.Parse("1/1110"), War = "b" },
            };
            repo.Setup(x => x.GetAll()).Returns(battles.AsQueryable());
            BattleLogic battleLogic = new BattleLogic(repo.Object);

            List<FirstBattleByWar> firstBattleByWars = new List<FirstBattleByWar>()
            {
                new FirstBattleByWar() { FirstWarDate = DateTime.Parse("1/1111"), Name = "a" },
                new FirstBattleByWar() { FirstWarDate = DateTime.Parse("1/1110"), Name = "b" },
            };

            var actual = battleLogic.FirstBattleByWar();

            Assert.That(actual, Is.EquivalentTo(firstBattleByWars));
            repo.Verify(x => x.GetAll(), Times.Exactly(1));
        }

        /// <summary>
        /// Test the CountGender Non-CRUD function.
        /// </summary>
        [Test]
        public void TestCountGender()
        {
            Mock<ICharacterRepository> repo = new Mock<ICharacterRepository>();
            List<Character> characters = new List<Character>()
            {
                new Character() { Gender = "a" },
                new Character() { Gender = "a" },
                new Character() { Gender = "a" },
                new Character() { Gender = "b" },
            };
            repo.Setup(x => x.GetAll()).Returns(characters.AsQueryable());
            CharacterLogic characterLogic = new CharacterLogic(repo.Object);

            List<CountGender> countGenders = new List<CountGender>()
            {
                new CountGender() { Gender = "a", Count = 3 },
                new CountGender() { Gender = "b", Count = 1 },
            };

            var actual = characterLogic.CountByGender();

            Assert.That(actual, Is.EquivalentTo(countGenders));
            repo.Verify(x => x.GetAll(), Times.Exactly(1));
        }

        /// <summary>
        /// Tests the CountByCategory Non-CRUD function.
        /// </summary>
        [Test]
        public void TestCountByCategory()
        {
            Mock<ILocationRepository> repo = new Mock<ILocationRepository>();
            List<Location> locations = new List<Location>()
            {
                new Location() { Category = "a", Dod = DateTime.Parse("1/1111") },
                new Location() { Category = "a", Dod = DateTime.Parse("1/1112") },
                new Location() { Category = "a" },
                new Location() { Category = "b", Dod = DateTime.Parse("1/1111") },
                new Location() { Category = "c" },
            };
            repo.Setup(x => x.GetAll()).Returns(locations.AsQueryable());
            LocationLogic locationLogic = new LocationLogic(repo.Object);

            List<CountByCategory> countByCategories = new List<CountByCategory>()
            {
                new CountByCategory() { Category = "a", Count = 2 },
                new CountByCategory() { Category = "b", Count = 1 },
            };

            var actual = locationLogic.CountingByCategory();
            Assert.That(actual, Is.EquivalentTo(countByCategories));
            repo.Verify(x => x.GetAll(), Times.Exactly(1));
        }

        /// <summary>
        /// Tests the SearchCharacterForSpecies Non-CRUD function.
        /// </summary>
        [Test]
        public void TestSearchCharacterForSpecies()
        {
            Mock<ICharacterRepository> repo = new Mock<ICharacterRepository>();
            List<Character> characters = new List<Character>()
            {
                new Character() { Species = "a", Name = "1" },
                new Character() { Species = "a", Name = "2" },
                new Character() { Species = "b", Name = "3" },
                new Character() { Species = "b", Name = "4" },
                new Character() { Species = "a", Name = "5" },
            };

            repo.Setup(x => x.GetAll()).Returns(characters.AsQueryable());
            CharacterLogic characterLogic = new CharacterLogic(repo.Object);

            var actual = characterLogic.SearchForSpecies("a");

            Assert.That(actual.Count, Is.EqualTo(3));
            Assert.That(actual.Select(x => x.Name), Does.Contain("1"));
            Assert.That(actual.Select(x => x.Name), Does.Contain("2"));
            Assert.That(actual.Select(x => x.Name), Does.Contain("5"));
            repo.Verify(x => x.GetAll(), Times.Exactly(1));
        }
    }
}
