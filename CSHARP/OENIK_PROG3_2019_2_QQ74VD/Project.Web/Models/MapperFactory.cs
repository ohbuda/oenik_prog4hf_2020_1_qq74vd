﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Project.Web.Models
{
    public class MapperFactory
    {
        public static IMapper CreateMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Project.Data.Character, Project.Web.Models.Character>().
                ForMember(dest => dest.Id, map => map.MapFrom(src => src.Id)).
                ForMember(dest => dest.Name, map => map.MapFrom(src => src.Name)).
                ForMember(dest => dest.Dob, map => map.MapFrom(src => src.Dob == null ? DateTime.MinValue : src.Dob)).
                ForMember(dest => dest.Dod, map => map.MapFrom(src => src.Dod == null ? DateTime.MinValue : src.Dod)).
                ForMember(dest => dest.Species, map => map.MapFrom(src => src.Species == null ? "" : src.Species)).
                ForMember(dest => dest.Gender, map => map.MapFrom(src => src.Gender == null ? "" : src.Gender)); ;

            });
            return config.CreateMapper();
        }
    }
}