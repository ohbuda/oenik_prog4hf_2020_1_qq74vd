﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Project.Web.Models
{
    public class CharactersViewModel
    {
        public Character EditedChar { get; set; }
        public List<Character> ListOfCharacters { get; set; }
    }
}