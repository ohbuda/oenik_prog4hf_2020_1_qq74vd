﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Project.Web.Models
{
    public class Character
    {
        [Display(Name = "Character ID")]
        [Required]
        public int Id { get; set; }

        [Display(Name = "Character Name")]
        [Required]
        [StringLength(30,MinimumLength = 3 )]
        public string Name { get; set; }

        [Display(Name = "Character Date of Birth")]
        [Required]
        public DateTime Dob { get; set; }

        [Display(Name = "Character Date of Death")]
        [Required]
        public DateTime Dod { get; set; }

        [Display(Name = "Character Species")]
        [Required]
        public string Species { get; set; }

        [Display(Name = "Character Gender")]
        [Required]
        public string Gender { get; set; }
    }
}