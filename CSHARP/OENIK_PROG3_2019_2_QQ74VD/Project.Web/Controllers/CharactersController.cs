﻿using AutoMapper;
using Project.Data;
using Project.Logic;
using Project.Repository;
using Project.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Project.Web.Controllers
{
    public class CharactersController : Controller
    {
        CharacterLogic logic;
        IMapper mapper;
        CharactersViewModel vm;

        public CharactersController()
        {
            CharacterRepository repo = new CharacterRepository(new DatabaseEntities7());
            logic = new CharacterLogic(repo);
            mapper = MapperFactory.CreateMapper();

            vm = new CharactersViewModel();
            vm.EditedChar = new Models.Character();
            var chars = logic.GetAll();
            vm.ListOfCharacters = mapper.Map<IEnumerable<Data.Character>, List<Models.Character>>(chars);
        }

        private Models.Character GetCharModel(int id)
        {
            return mapper.Map<Data.Character, Models.Character>(logic.GetById(id));
        }

        // GET: Characters
        public ActionResult Index()
        {
            ViewData["editAction"] = "AddNew";
            return View("CharactersIndex", vm);
        }

        // GET: Characters/Details/5
        public ActionResult Details(int id)
        {
            return View("CharactersDetails", GetCharModel(id));
        }

        // GET: Characters/Create
        public ActionResult Create()
        {
            return View();
        }

        public ActionResult Remove(int id)
        {
            TempData["editResult"] = "Delete FAIL";
            if(logic.Delete(id)) TempData["editResult"] = "Delete OK";
            return RedirectToAction("Index");
        }

        public ActionResult Edit(int id)
        {
            ViewData["editAction"] = "Edit";
            vm.EditedChar = GetCharModel(id);
            return View("CharactersIndex", vm);

        }
        
        [HttpPost]
        public ActionResult Edit(Models.Character c, string editAction)
        {
            if (ModelState.IsValid && c != null)
            {
                TempData["editResult"] = "Edit OK";
                if(editAction == "AddNew")
                {
                    logic.Add(new Data.Character()
                    {
                        Name = c.Name,
                        Species = c.Species,
                        Dob = c.Dob,
                        Dod = c.Dod,
                        Gender = c.Gender,
                    });
                }
                else
                {
                    bool ret = logic.Update(c.Id, c.Name, c.Gender, c.Species, c.Dob, c.Dod);
                    //if (!ret) TempData["editResult"] = "Edit FAIL";
                }
                return RedirectToAction("Index");
            }
            else
            {
                ViewData["editAction"] = "Edit";
                vm.EditedChar = c;
                return View("CharactersIndex", vm);
            }
        }
    }
}
