﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Project.Web.Controllers
{
    using System.CodeDom;
    using System.Data.Entity;

    using AutoMapper;

    using Project.Data;
    using Project.Logic;
    using Project.Repository;
    using Project.Web.Models;

    public class CharactersAPIController : ApiController
    {
        public class ApiResult
        {
            public bool OperationResult { get; set; }
        }

        private CharacterLogic logic;

        private IMapper mapper;

        public CharactersAPIController()
        {
            CharacterRepository repo = new CharacterRepository(new DatabaseEntities7());
            logic = new CharacterLogic(repo);
            mapper = MapperFactory.CreateMapper();
        }

        [ActionName("all")]
        [HttpGet]
        public IEnumerable<Models.Character> GetAll()
        {
            var characters = logic.GetAll().ToList();
            return mapper.Map<IList<Data.Character>, List<Models.Character>>(characters);
        }

        [ActionName("del")]
        [HttpGet]
        public ApiResult DelOne(int id)
        {
            var result = logic.Delete(id);
            return new ApiResult() { OperationResult = result };
        }

        [ActionName("add")]
        [HttpPost]
        public ApiResult AddOne(Models.Character c)
        {
            var value = logic.AddConfirm(new Data.Character()
                          {
                              Name = c.Name,
                              Species = c.Species,
                              Dob = c.Dob,
                              Dod = c.Dod,
                              Gender = c.Gender,
                          });
            return new ApiResult() { OperationResult = value };

        }

        [ActionName("mod")]
        [HttpPost]
        public ApiResult ModOne(Models.Character c)
        {
            var result = logic.Update(c.Id, c.Name, c.Gender, c.Species, c.Dob, c.Dod);
            return new ApiResult() { OperationResult = result };
        }  
    }
}
