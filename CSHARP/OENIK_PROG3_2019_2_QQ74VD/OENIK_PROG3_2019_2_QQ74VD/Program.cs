﻿// <copyright file="Program.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OENIK_PROG3_2019_2_QQ74VD
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Threading.Tasks;
    using System.Xml.Linq;
    using Project.Data;
    using Project.Logic;

    /// <summary>
    /// Main Program of the project. Includes a menu with which you can navigate through
    /// the functions defined in the Logic layer.
    /// </summary>
    internal class Program
    {
        /// <summary>
        /// Main menu of program. Used to access submenus.
        /// </summary>
        private static void Main()
        {
            bool inProgram = true;
            while (inProgram)
            {
                Console.WriteLine("1, List files");
                Console.WriteLine("2, Add");
                Console.WriteLine("3, Remove");
                Console.WriteLine("4, Modify");
                Console.WriteLine("5, Java Endpoint");
                Console.WriteLine("6, Non-CRUD operations.");
                Console.WriteLine("7, Exit");
                string s = Console.ReadLine();
                switch (s)
                {
                    case "1":
                        ListTables();
                        break;
                    case "2":
                        AddToTable();
                        break;
                    case "3":
                        MenuRemoveFromTable();
                        break;
                    case "4":
                        ListModifies();
                        break;
                    case "5":
                        JavaEndpoint();
                        break;
                    case "6":
                        NonCRUD();
                        break;
                    case "7":
                        Console.WriteLine("Exiting Program");
                        inProgram = false;
                        break;
                    default:
                        Console.WriteLine("Waiting for correct input.");
                        break;
                }

                s = string.Empty;
            }
        }

        /// <summary>
        /// Implementation of Listing of tables from Logic.
        /// </summary>
        private static void ListTables()
        {
            Console.WriteLine("Battles");
            BattleLogic battleLogic = new BattleLogic();
            foreach (Battle battle in battleLogic.GetAll())
            {
                Console.WriteLine(battle.ToString());
            }

            Console.ReadLine();
            Console.WriteLine("Characters");
            CharacterLogic characterLogic = new CharacterLogic();
            foreach (Character character in characterLogic.GetAll())
            {
                Console.WriteLine(character.ToString());
            }

            Console.ReadLine();
            Console.WriteLine("Locations");
            LocationLogic locationLogic = new LocationLogic();
            foreach (Location location in locationLogic.GetAll())
            {
                Console.WriteLine(location.ToString());
            }

            Console.WriteLine("All tables listed. Returning to main menu.");
            Console.ReadLine();
        }

        /// <summary>
        /// Menu of adding opertaions for tables.
        /// </summary>
        private static void AddToTable()
        {
            bool inProgram = true;
            while (inProgram)
            {
                Console.WriteLine("Select table:");
                Console.WriteLine("1, Battles");
                Console.WriteLine("2, Characters");
                Console.WriteLine("3, Locations");
                Console.WriteLine("4, Exit");
                string s = Console.ReadLine();
                switch (s)
                {
                    case "1":
                        AddToBattle();
                        break;
                    case "2":
                        AddToChar();
                        break;
                    case "3":
                        AddToLocation();
                        break;
                    case "4":
                        Console.WriteLine("Returning to main menu.");
                        inProgram = false;
                        break;
                    default:
                        Console.WriteLine("Waiting for correct input.");
                        break;
                }

                s = string.Empty;
            }
        }

        /// <summary>
        /// Implementation of adding to Battle table from Logic. Asks for all fields of the table,
        /// makes sure the input is correct.
        /// </summary>
        private static void AddToBattle()
        {
            int id = IdChecker();

            string name = NameChecker();

            Console.Write("Date of starting (in M/YYYY):");
            DateTime dos = DateChecker();

            Console.Write("Date of ending (in M/YYYY):");
            DateTime doe = DateChecker();

            Console.Write("War:");
            string war = Console.ReadLine();

            Console.Write("Outcome:");
            string outcome = Console.ReadLine();

            BattleLogic battleLogic = new BattleLogic();

            try
            {
                battleLogic.Add(new Battle { Id = id, Doe = doe, Dos = dos, Name = name, Outcome = outcome, War = war });
                Console.WriteLine("Succesfully added to table.");
            }
            catch (ArgumentException)
            {
                Console.WriteLine("ID already exists in table.");
            }

            Console.WriteLine("Returning to previous menu.");
        }

        /// <summary>
        /// Implementation of adding to Character table from Logic. Asks for all fields of the table,
        /// makes sure the input is correct.
        /// </summary>
        private static void AddToChar()
        {
            int id = IdChecker();

            string name = NameChecker();

            Console.Write("Date of birth (in M/YYYY):");
            DateTime dob = DateChecker();

            Console.Write("Date of death (in M/YYYY):");
            DateTime dod = DateChecker();

            Console.Write("Species:");
            string species = Console.ReadLine();

            Console.Write("Gender:");
            string gender = Console.ReadLine();

            CharacterLogic characterLogic = new CharacterLogic();

            try
            {
                characterLogic.Add(new Character { Id = id, Dod = dod, Dob = dob, Name = name, Species = species, Gender = gender });
                Console.WriteLine("Succesfully added to table.");
            }
            catch (ArgumentException)
            {
                Console.WriteLine("ID already exists in table.");
            }

            Console.WriteLine("Returning to previous menu.");
        }

        /// <summary>
        /// Implementation of adding to Location table from Logic. Asks for all fields of the table,
        /// makes sure the input is correct.
        /// </summary>
        private static void AddToLocation()
        {
            int id = IdChecker();

            string name = NameChecker();

            Console.Write("Date of founding (in M/YYYY):");
            DateTime dof = DateChecker();

            Console.Write("Date of destruction (in M/YYYY):");
            DateTime dod = DateChecker();

            Console.Write("Region:");
            string region = Console.ReadLine();

            Console.Write("Category:");
            string category = Console.ReadLine();

            LocationLogic locationLogic = new LocationLogic();

            try
            {
                locationLogic.Add(new Location { Id = id, Dod = dod, Dof = dof, Name = name, Region = region, Category = category });
                Console.WriteLine("Succesfully added to table.");
            }
            catch (ArgumentException)
            {
                Console.WriteLine("ID already exists in table.");
            }

            Console.WriteLine("Returning to previous menu.");
        }

        /// <summary>
        /// Menu of remove operations for tables.
        /// </summary>
        private static void MenuRemoveFromTable()
        {
            int id = 0;
            bool inProgram = true;
            while (inProgram)
            {
                bool invalidId = true;
                Console.WriteLine("Select table:");
                Console.WriteLine("1, Battles");
                Console.WriteLine("2, Characters");
                Console.WriteLine("3, Locations");
                Console.WriteLine("4, Exit");
                string s = Console.ReadLine();
                switch (s)
                {
                    case "1":
                        Console.Write("Enter ID of entry.");
                        while (invalidId)
                        {
                            Console.Write("ID:");
                            try
                            {
                                id = int.Parse(Console.ReadLine());
                                BattleLogic logic = new BattleLogic();
                                logic.Delete(logic.GetById(id));
                                invalidId = false;
                            }
                            catch (FormatException)
                            {
                                Console.WriteLine("Invalid ID, please reenter.");
                            }
                        }

                        break;
                    case "2":
                        Console.Write("Enter ID of entry:");
                        while (invalidId)
                        {
                            Console.Write("ID:");
                            try
                            {
                                id = int.Parse(Console.ReadLine());
                                CharacterLogic logic = new CharacterLogic();
                                logic.Delete(logic.GetById(id));
                                invalidId = false;
                            }
                            catch (FormatException)
                            {
                                Console.WriteLine("Invalid ID, please reenter.");
                            }
                        }

                        break;
                    case "3":
                        Console.Write("Enter ID of entry.");
                        while (invalidId)
                        {
                            Console.Write("ID:");
                            try
                            {
                                id = int.Parse(Console.ReadLine());
                                LocationLogic logic = new LocationLogic();
                                logic.Delete(logic.GetById(id));
                                invalidId = false;
                            }
                            catch (FormatException)
                            {
                                Console.WriteLine("Invalid ID, please reenter.");
                            }
                        }

                        break;
                    case "4":
                        Console.WriteLine("Returning to main menu.");
                        inProgram = false;
                        break;
                    default:
                        Console.WriteLine("Waiting for correct input.");
                        break;
                }

                s = string.Empty;
            }
        }

        /// <summary>
        /// Menu of modify operations for tables. Asks for an ID, removes if ID exists.
        /// </summary>
        private static void ListModifies()
        {
            bool inProgram = true;
            while (inProgram)
            {
                int id = 0;
                string st = string.Empty;
                Console.WriteLine("1, Battles: Change Outcome");
                Console.WriteLine("2, Character: Change Name");
                Console.WriteLine("3, Location: Change Category");
                Console.WriteLine("4, Exit");
                string s = Console.ReadLine();
                switch (s)
                {
                    case "1":
                        BattleLogic battleLogic = new BattleLogic();

                        id = IdChecker();

                        Console.WriteLine("New outcome:");
                        st = Console.ReadLine();
                        try
                        {
                            battleLogic.UpdateOutcome(id, st);
                        }
                        catch (NullReferenceException)
                        {
                            Console.WriteLine("ID doesn't exist in table");
                        }

                        break;
                    case "2":
                        CharacterLogic characterLogic = new CharacterLogic();

                        id = IdChecker();

                        Console.WriteLine("New name:");
                        st = Console.ReadLine();
                        try
                        {
                            characterLogic.UpdateName(id, st);
                        }
                        catch (NullReferenceException)
                        {
                            Console.WriteLine("ID doesn't exist in table");
                        }

                        break;
                    case "3":
                        LocationLogic locationLogic = new LocationLogic();

                        id = IdChecker();

                        Console.WriteLine("New category:");
                        st = Console.ReadLine();
                        try
                        {
                            locationLogic.UpdateCategory(id, st);
                        }
                        catch (NullReferenceException)
                        {
                            Console.WriteLine("ID doesn't exist in table");
                        }

                        break;
                    case "4":
                        Console.WriteLine("Exiting Program");
                        inProgram = false;
                        break;
                    default:
                        Console.WriteLine("Waiting for correct input.");
                        break;
                }

                s = string.Empty;
            }
        }

        /// <summary>
        /// Implementation of java endpoint. Writes out randomly made data for a new Battle row.
        /// </summary>
        private static void JavaEndpoint()
        {
            WebClient wc = new WebClient();
            try
            {
                Console.WriteLine(wc.DownloadString("http://localhost:8080/JavaProjectServer/Servlet"));
                wc.Dispose();
            }
            catch (WebException)
            {
                Console.WriteLine("Java Endpoint not open.");
                wc.Dispose();
            }
        }

        /// <summary>
        /// Checks if ID input is proper.
        /// </summary>
        /// <returns>Returns an int if a proper input is found.</returns>
        private static int IdChecker()
        {
            bool invalidId = true;
            int id = 0;
            while (invalidId)
            {
                Console.Write("ID:");
                try
                {
                    id = int.Parse(Console.ReadLine());
                    invalidId = false;
                }
                catch (ArgumentNullException)
                {
                    Console.WriteLine("Invalid ID, please reenter.");
                }
                catch (FormatException)
                {
                    Console.WriteLine("Invalid ID, please reenter.");
                }
            }

            return id;
        }

        /// <summary>
        /// Checks if Name input is proper.
        /// </summary>
        /// <returns>Returns an string if a proper input is found.</returns>
        private static string NameChecker()
        {
            Console.Write("Name:");
            string name = Console.ReadLine();
            while (name == string.Empty)
            {
                Console.WriteLine("Invalid input, please reenter.");
                Console.Write("Name:");
                name = Console.ReadLine();
            }

            return name;
        }

        /// <summary>
        /// Checks if Date input is proper.
        /// </summary>
        /// <returns>Returns an Datetime if a proper input is found.</returns>
        private static DateTime DateChecker()
        {
            bool invalidDos = true;
            DateTime dos = DateTime.MinValue;
            while (invalidDos)
            {
                try
                {
                    dos = DateTime.Parse(Console.ReadLine());
                    invalidDos = false;
                }
                catch (ArgumentNullException)
                {
                    Console.WriteLine("Invalid input, please reenter.");
                }
                catch (FormatException)
                {
                    Console.WriteLine("Invalid input, please reenter.");
                }
            }

            return dos;
        }

        /// <summary>
        /// Menu of Non-CRUD operations.
        /// </summary>
        private static void NonCRUD()
        {
            bool menuLoop = true;
            while (menuLoop)
            {
                Console.WriteLine("1, Show every war and the date of the first battle");
                Console.WriteLine("2, Show every gender and their counts.");
                Console.WriteLine("3, Show every category and the count of destroyed locations.");
                Console.WriteLine("4, Search Characters table by given input");
                Console.WriteLine("5, Exit");
                string s = Console.ReadLine();
                switch (s)
                {
                    case "1":
                        BattleLogic battleLogic = new BattleLogic();
                        var q = battleLogic.FirstBattleByWar();
                        foreach (var item in q)
                        {
                           Console.WriteLine(item.ToString());
                        }

                        break;
                    case "2":
                        CharacterLogic characterLogic = new CharacterLogic();

                        var q2 = characterLogic.CountByGender();
                        foreach (var item in q2)
                        {
                            Console.WriteLine(item.ToString());
                        }

                        break;
                    case "3":
                        LocationLogic locationLogic = new LocationLogic();

                        var q3 = locationLogic.CountingByCategory();
                        foreach (var item in q3)
                        {
                        Console.WriteLine(item.ToString());
                        }

                        break;
                    case "4":
                        Console.Write("Input species of character to be searched for:");
                        string s2 = Console.ReadLine();

                        CharacterLogic characterLogic2 = new CharacterLogic();
                        var q4 = characterLogic2.SearchForSpecies(s2);

                        foreach (var item in q4)
                        {
                            Console.WriteLine(item.ToString());
                        }

                        break;
                    case "5":
                        Console.WriteLine("Exiting");
                        menuLoop = false;
                        break;
                    default:
                        Console.WriteLine("Incorrect input.");
                        break;
                }
            }
        }
    }
}
