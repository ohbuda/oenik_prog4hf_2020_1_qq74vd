﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.WPF
{
    class CharacterVM : ObservableObject
    {
        private int id;
        private string name;
        private string species;
        private string gender;
        private DateTime dob;
        private DateTime dod;

        public string Species
        {
            get { return species; }
            set { Set(ref species, value); }
        }

        public string Gender
        {
            get { return gender; }
            set { Set(ref gender, value); }
        }

        public DateTime Dob
        {
            get { return dob; }
            set { Set(ref dob, value); }
        }

        public DateTime Dod
        {
            get { return dod; }
            set { Set(ref dod, value); }
        }

        public string Name
        {
            get { return name; }
            set { Set(ref name, value); }
        }

        public int Id
        {
            get { return id; }
            set { Set(ref id, value); }
        }

        public void CopyFrom(CharacterVM c)
        {
            if (c == null) return;
            this.id = c.id;
            this.species = c.Species;
            this.gender = c.Gender;
            this.name = c.Name;
            this.dob = c.Dob;
            this.dod = c.Dod;
        }
    }
}
