﻿using GalaSoft.MvvmLight.Messaging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Project.WPF
{
    class MainLogic
    {
        string url = "http://localhost:49984/api/CharactersAPI/";
        HttpClient client = new HttpClient();

        void SendMessage(bool success)
        {
            string msg = success ? "OK" : "NOT OK!!!";
            Messenger.Default.Send(msg, "CharacterResult");
        }

        public List<CharacterVM> ApiGetCharacters()
        {
            string json = client.GetStringAsync(url + "all").Result;
            var list = JsonConvert.DeserializeObject<List<CharacterVM>>(json);
            return list;
        }

        public void ApiDelCharacter(CharacterVM c)
        {
            bool success = false;
            if (c != null)
            {
                string json = client.GetStringAsync(url + "del/" + c.Id).Result;
                JObject obj = JObject.Parse(json);
                success = (bool)obj["OperationResult"];
            }
            SendMessage(success);
        }

        bool ApiEditCharacter(CharacterVM c, bool isEditing)
        {
            if (c == null) return false;
            string myURl = isEditing ? url + "mod" : url + "add";

            Dictionary<string, string> postData = new Dictionary<string, string>();
            if (isEditing) postData.Add(nameof(CharacterVM.Id), c.Id.ToString());
            postData.Add(nameof(CharacterVM.Name), c.Name);
            postData.Add(nameof(CharacterVM.Species), c.Species);
            postData.Add(nameof(CharacterVM.Gender), c.Gender);
            postData.Add(nameof(CharacterVM.Dob), c.Dob.ToString());
            postData.Add(nameof(CharacterVM.Dod), c.Dod.ToString());
            string json = client.PostAsync(myURl, new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;
            JObject obj = JObject.Parse(json);
            return (bool)obj["OperationResult"];
        }

        public void EditCharacter(CharacterVM c, Func<CharacterVM, bool> editor)
        {
            CharacterVM clone = new CharacterVM();
            if (c != null) clone.CopyFrom(c);
            bool? success = editor?.Invoke(clone);
            if (success == true)
            {
                if (c != null) ApiEditCharacter(clone, true);
                else ApiEditCharacter(clone, false);
            }
            SendMessage(success == true);
        }
    }
}
