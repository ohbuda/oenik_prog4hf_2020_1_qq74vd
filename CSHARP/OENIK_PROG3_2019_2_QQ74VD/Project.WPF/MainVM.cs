﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Project.WPF
{
    class MainVM : ViewModelBase
    {
        private MainLogic logic;
        private ObservableCollection<CharacterVM> allCharacters;
        private CharacterVM selectedCharacter;

        public CharacterVM SelectedCharacter
        {
            get { return selectedCharacter; }
            set { Set(ref selectedCharacter, value); }
        }


        public ObservableCollection<CharacterVM> AllCharacters
        {
            get { return allCharacters; }
            set { Set(ref allCharacters, value); }
        }

        public ICommand AddCmd { get; private set; }
        public ICommand ModCmd { get; private set; }
        public ICommand DelCmd { get; private set; }
        public ICommand ListCmd { get; private set; }

        public Func<CharacterVM,bool> EditorFunc { get; set; }

        public MainVM()
        {
            logic = new MainLogic();
            ListCmd = new RelayCommand(() => AllCharacters = new ObservableCollection<CharacterVM>(logic.ApiGetCharacters()));
            DelCmd = new RelayCommand(() => logic.ApiDelCharacter(selectedCharacter));
            ModCmd = new RelayCommand(() => logic.EditCharacter(selectedCharacter, EditorFunc));
            AddCmd = new RelayCommand(() => logic.EditCharacter(null, EditorFunc));
        }
    }
}
