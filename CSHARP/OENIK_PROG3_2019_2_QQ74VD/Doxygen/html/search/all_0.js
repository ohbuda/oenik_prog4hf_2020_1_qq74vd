var searchData=
[
  ['add_0',['Add',['../class_project_1_1_logic_1_1_battle_logic.html#ac9d31dd22434df26c441e2a011a74dec',1,'Project.Logic.BattleLogic.Add()'],['../class_project_1_1_logic_1_1_character_logic.html#ac2072c463a13303d6d95437dfdfc448f',1,'Project.Logic.CharacterLogic.Add()'],['../interface_project_1_1_logic_1_1_i_logic.html#a06daa6f63e7135387ff1b7be22acf574',1,'Project.Logic.ILogic.Add()'],['../class_project_1_1_logic_1_1_location_logic.html#af647f18f2f68d9b6c94b00fa8303e215',1,'Project.Logic.LocationLogic.Add()']]],
  ['addingtodatabasebattles_1',['AddingToDatabaseBattles',['../class_project_1_1_logic_1_1_tests_1_1_test_class.html#ae3ad9085968783d4cbdd83b053585c2f',1,'Project::Logic::Tests::TestClass']]],
  ['addingtodatabasebattlescharacters_2',['AddingToDatabaseBattlesCharacters',['../class_project_1_1_logic_1_1_tests_1_1_test_class.html#a9f27daf741ccfa9e763d0139ee3050f2',1,'Project::Logic::Tests::TestClass']]],
  ['addingtodatabaselocations_3',['AddingToDatabaseLocations',['../class_project_1_1_logic_1_1_tests_1_1_test_class.html#a6d261726e9c008c87c65a239f5974e9a',1,'Project::Logic::Tests::TestClass']]],
  ['analyzer_20configuration_4',['Analyzer Configuration',['../md_prog3project__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__q_q74_v_d_packages__microsoft_8_code_a35b54c98b2194ad332e5c3753c717d2e.html',1,'']]]
];
