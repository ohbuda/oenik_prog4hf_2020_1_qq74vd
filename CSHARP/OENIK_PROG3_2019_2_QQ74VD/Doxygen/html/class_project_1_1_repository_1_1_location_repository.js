var class_project_1_1_repository_1_1_location_repository =
[
    [ "LocationRepository", "class_project_1_1_repository_1_1_location_repository.html#aadebf7e5bfe1f2649c0d9b156841ef3d", null ],
    [ "Create", "class_project_1_1_repository_1_1_location_repository.html#a1dd6d1c4bd6c431febcf1a3b5c3a58fb", null ],
    [ "Delete", "class_project_1_1_repository_1_1_location_repository.html#a923902808ed06303709aac3748438848", null ],
    [ "GetAll", "class_project_1_1_repository_1_1_location_repository.html#a63041d26dd1c9f99274803bebef10510", null ],
    [ "GetById", "class_project_1_1_repository_1_1_location_repository.html#a51da16bb245fce3fd88356ee6e24b062", null ],
    [ "UpdateCategory", "class_project_1_1_repository_1_1_location_repository.html#a25fcd46f9a166e17320addb656148b38", null ]
];