var class_project_1_1_logic_1_1_tests_1_1_test_class =
[
    [ "AddingToDatabaseBattles", "class_project_1_1_logic_1_1_tests_1_1_test_class.html#ae3ad9085968783d4cbdd83b053585c2f", null ],
    [ "AddingToDatabaseBattlesCharacters", "class_project_1_1_logic_1_1_tests_1_1_test_class.html#a9f27daf741ccfa9e763d0139ee3050f2", null ],
    [ "AddingToDatabaseLocations", "class_project_1_1_logic_1_1_tests_1_1_test_class.html#a6d261726e9c008c87c65a239f5974e9a", null ],
    [ "ReadAllBattles", "class_project_1_1_logic_1_1_tests_1_1_test_class.html#a279199380c6c32765015d9f1f3e23fa9", null ],
    [ "ReadAllCharacters", "class_project_1_1_logic_1_1_tests_1_1_test_class.html#ab5607be3edb617dbdf1347b1d25193f0", null ],
    [ "ReadAllLocations", "class_project_1_1_logic_1_1_tests_1_1_test_class.html#a694ff60ce93dd910a655e718e6f3861e", null ],
    [ "RemoveFromDatabaseBattles", "class_project_1_1_logic_1_1_tests_1_1_test_class.html#acd15727e3f7861c050d8295d508c73bf", null ],
    [ "RemoveFromDatabaseCharacters", "class_project_1_1_logic_1_1_tests_1_1_test_class.html#a9f9bcb817a26526f2088865fa9c962e3", null ],
    [ "RemoveFromDatabaseLocations", "class_project_1_1_logic_1_1_tests_1_1_test_class.html#a1033c02e88f48b76325b9811e37577b1", null ],
    [ "TestCountByCategory", "class_project_1_1_logic_1_1_tests_1_1_test_class.html#a20646a4780dc431778d19c4063a4ba9e", null ],
    [ "TestCountGender", "class_project_1_1_logic_1_1_tests_1_1_test_class.html#aa062c0e025d8092b8a3f6bc091947471", null ],
    [ "TestFirstBattleByWar", "class_project_1_1_logic_1_1_tests_1_1_test_class.html#ac5221809cd0df6771fd0f33358103bf0", null ],
    [ "TestSearchCharacterForSpecies", "class_project_1_1_logic_1_1_tests_1_1_test_class.html#aa4a62dc5c7e538910975bb80c7389d9e", null ],
    [ "UpdateCategory", "class_project_1_1_logic_1_1_tests_1_1_test_class.html#ae2c09ee6d16a5c390e99c955ee568c34", null ],
    [ "UpdateName", "class_project_1_1_logic_1_1_tests_1_1_test_class.html#aa6820defeeb2887e68e4e35bf41374ba", null ],
    [ "UpdateOutcome", "class_project_1_1_logic_1_1_tests_1_1_test_class.html#a907cbcc81a22fd82cda0609dbe064cd5", null ]
];