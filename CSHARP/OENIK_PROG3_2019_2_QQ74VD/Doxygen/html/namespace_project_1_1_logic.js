var namespace_project_1_1_logic =
[
    [ "Tests", "namespace_project_1_1_logic_1_1_tests.html", "namespace_project_1_1_logic_1_1_tests" ],
    [ "BattleLogic", "class_project_1_1_logic_1_1_battle_logic.html", "class_project_1_1_logic_1_1_battle_logic" ],
    [ "CharacterLogic", "class_project_1_1_logic_1_1_character_logic.html", "class_project_1_1_logic_1_1_character_logic" ],
    [ "CountByCategory", "class_project_1_1_logic_1_1_count_by_category.html", "class_project_1_1_logic_1_1_count_by_category" ],
    [ "CountGender", "class_project_1_1_logic_1_1_count_gender.html", "class_project_1_1_logic_1_1_count_gender" ],
    [ "FirstBattleByWar", "class_project_1_1_logic_1_1_first_battle_by_war.html", "class_project_1_1_logic_1_1_first_battle_by_war" ],
    [ "IBattleLogic", "interface_project_1_1_logic_1_1_i_battle_logic.html", "interface_project_1_1_logic_1_1_i_battle_logic" ],
    [ "ICharacterLogic", "interface_project_1_1_logic_1_1_i_character_logic.html", "interface_project_1_1_logic_1_1_i_character_logic" ],
    [ "ILocationLogic", "interface_project_1_1_logic_1_1_i_location_logic.html", "interface_project_1_1_logic_1_1_i_location_logic" ],
    [ "ILogic", "interface_project_1_1_logic_1_1_i_logic.html", "interface_project_1_1_logic_1_1_i_logic" ],
    [ "LocationLogic", "class_project_1_1_logic_1_1_location_logic.html", "class_project_1_1_logic_1_1_location_logic" ]
];