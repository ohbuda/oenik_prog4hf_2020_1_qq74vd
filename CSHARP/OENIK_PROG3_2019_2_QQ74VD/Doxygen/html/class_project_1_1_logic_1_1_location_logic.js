var class_project_1_1_logic_1_1_location_logic =
[
    [ "LocationLogic", "class_project_1_1_logic_1_1_location_logic.html#acfa5002a3d8265dc2765f578d9958d24", null ],
    [ "LocationLogic", "class_project_1_1_logic_1_1_location_logic.html#ac27d30247786b9fdff8f5aeb6f0d1b1b", null ],
    [ "Add", "class_project_1_1_logic_1_1_location_logic.html#af647f18f2f68d9b6c94b00fa8303e215", null ],
    [ "CountingByCategory", "class_project_1_1_logic_1_1_location_logic.html#a7930af9e26e2022ebfbb3763511e231b", null ],
    [ "Delete", "class_project_1_1_logic_1_1_location_logic.html#ad6813c4499bb9605572ef7352732256d", null ],
    [ "GetAll", "class_project_1_1_logic_1_1_location_logic.html#ade23daeb721e2fed894e074da624e9b8", null ],
    [ "GetById", "class_project_1_1_logic_1_1_location_logic.html#a62b366b48048cf66d628ae98778bb5ba", null ],
    [ "UpdateCategory", "class_project_1_1_logic_1_1_location_logic.html#a98a283acb407a92b0f73d071fb4bfa27", null ]
];