var hierarchy =
[
    [ "Project.Data.Battle", "class_project_1_1_data_1_1_battle.html", null ],
    [ "Project.Data.Character", "class_project_1_1_data_1_1_character.html", null ],
    [ "Project.Data.Con_BattToLoc", "class_project_1_1_data_1_1_con___batt_to_loc.html", null ],
    [ "Project.Data.Con_CharToBatt", "class_project_1_1_data_1_1_con___char_to_batt.html", null ],
    [ "Project.Data.Con_CharToChar", "class_project_1_1_data_1_1_con___char_to_char.html", null ],
    [ "Project.Logic.CountByCategory", "class_project_1_1_logic_1_1_count_by_category.html", null ],
    [ "Project.Logic.CountGender", "class_project_1_1_logic_1_1_count_gender.html", null ],
    [ "DbContext", null, [
      [ "Project.Data::DatabaseEntities7", "class_project_1_1_data_1_1_database_entities7.html", null ]
    ] ],
    [ "Project.Logic.FirstBattleByWar", "class_project_1_1_logic_1_1_first_battle_by_war.html", null ],
    [ "Project.Logic.ILogic< T >", "interface_project_1_1_logic_1_1_i_logic.html", null ],
    [ "Project.Logic.ILogic< Battle >", "interface_project_1_1_logic_1_1_i_logic.html", [
      [ "Project.Logic.IBattleLogic", "interface_project_1_1_logic_1_1_i_battle_logic.html", [
        [ "Project.Logic.BattleLogic", "class_project_1_1_logic_1_1_battle_logic.html", null ]
      ] ]
    ] ],
    [ "Project.Logic.ILogic< Character >", "interface_project_1_1_logic_1_1_i_logic.html", [
      [ "Project.Logic.ICharacterLogic", "interface_project_1_1_logic_1_1_i_character_logic.html", [
        [ "Project.Logic.CharacterLogic", "class_project_1_1_logic_1_1_character_logic.html", null ]
      ] ]
    ] ],
    [ "Project.Logic.ILogic< Location >", "interface_project_1_1_logic_1_1_i_logic.html", [
      [ "Project.Logic.ILocationLogic", "interface_project_1_1_logic_1_1_i_location_logic.html", [
        [ "Project.Logic.LocationLogic", "class_project_1_1_logic_1_1_location_logic.html", null ]
      ] ]
    ] ],
    [ "Project.Repository.IRepository< TEntity >", "interface_project_1_1_repository_1_1_i_repository.html", [
      [ "Project.Repository.Repository< TEntity >", "class_project_1_1_repository_1_1_repository.html", null ]
    ] ],
    [ "Project.Repository.IRepository< Battle >", "interface_project_1_1_repository_1_1_i_repository.html", [
      [ "Project.Repository.IBattleRepository", "interface_project_1_1_repository_1_1_i_battle_repository.html", [
        [ "Project.Repository.BattleRepository", "class_project_1_1_repository_1_1_battle_repository.html", null ]
      ] ]
    ] ],
    [ "Project.Repository.IRepository< Character >", "interface_project_1_1_repository_1_1_i_repository.html", [
      [ "Project.Repository.ICharacterRepository", "interface_project_1_1_repository_1_1_i_character_repository.html", [
        [ "Project.Repository.CharacterRepository", "class_project_1_1_repository_1_1_character_repository.html", null ]
      ] ]
    ] ],
    [ "Project.Repository.IRepository< Location >", "interface_project_1_1_repository_1_1_i_repository.html", [
      [ "Project.Repository.ILocationRepository", "interface_project_1_1_repository_1_1_i_location_repository.html", [
        [ "Project.Repository.LocationRepository", "class_project_1_1_repository_1_1_location_repository.html", null ]
      ] ]
    ] ],
    [ "Project.Data.Location", "class_project_1_1_data_1_1_location.html", null ],
    [ "OENIK_PROG3_2019_2_QQ74VD.Program", "class_o_e_n_i_k___p_r_o_g3__2019__2___q_q74_v_d_1_1_program.html", null ],
    [ "Project.Repository.Repository< Battle >", "class_project_1_1_repository_1_1_repository.html", [
      [ "Project.Repository.BattleRepository", "class_project_1_1_repository_1_1_battle_repository.html", null ]
    ] ],
    [ "Project.Repository.Repository< Character >", "class_project_1_1_repository_1_1_repository.html", [
      [ "Project.Repository.CharacterRepository", "class_project_1_1_repository_1_1_character_repository.html", null ]
    ] ],
    [ "Project.Repository.Repository< Location >", "class_project_1_1_repository_1_1_repository.html", [
      [ "Project.Repository.LocationRepository", "class_project_1_1_repository_1_1_location_repository.html", null ]
    ] ],
    [ "Project.Logic.Tests.TestClass", "class_project_1_1_logic_1_1_tests_1_1_test_class.html", null ]
];