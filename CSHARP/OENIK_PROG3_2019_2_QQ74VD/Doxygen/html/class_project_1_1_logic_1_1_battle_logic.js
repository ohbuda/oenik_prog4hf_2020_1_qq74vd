var class_project_1_1_logic_1_1_battle_logic =
[
    [ "BattleLogic", "class_project_1_1_logic_1_1_battle_logic.html#af26987d346a6629097d0a4741d5b7925", null ],
    [ "BattleLogic", "class_project_1_1_logic_1_1_battle_logic.html#a4f1b92d6095b8af9bf2b8530cf0fe2e7", null ],
    [ "Add", "class_project_1_1_logic_1_1_battle_logic.html#ac9d31dd22434df26c441e2a011a74dec", null ],
    [ "Delete", "class_project_1_1_logic_1_1_battle_logic.html#ae8040313c44731738a19c1744f4cdfd1", null ],
    [ "FirstBattleByWar", "class_project_1_1_logic_1_1_battle_logic.html#a4b413b1f45feaa004610bd5a1c1f2aa9", null ],
    [ "GetAll", "class_project_1_1_logic_1_1_battle_logic.html#a46c236d3aa6a8edb92a3c3cc9c013641", null ],
    [ "GetById", "class_project_1_1_logic_1_1_battle_logic.html#acb81df299be8bb15b2d8302cd33e289b", null ],
    [ "UpdateOutcome", "class_project_1_1_logic_1_1_battle_logic.html#a0036abfc0fda9af36dd8a0df2b467971", null ]
];