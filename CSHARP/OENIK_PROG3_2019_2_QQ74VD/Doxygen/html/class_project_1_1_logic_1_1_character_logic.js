var class_project_1_1_logic_1_1_character_logic =
[
    [ "CharacterLogic", "class_project_1_1_logic_1_1_character_logic.html#afa1cc5e21d9ce71eabb0f8ded5c53e35", null ],
    [ "CharacterLogic", "class_project_1_1_logic_1_1_character_logic.html#a3df2dee0d2294fbe2fad675bb20f5c51", null ],
    [ "Add", "class_project_1_1_logic_1_1_character_logic.html#ac2072c463a13303d6d95437dfdfc448f", null ],
    [ "CountByGender", "class_project_1_1_logic_1_1_character_logic.html#aca04073fb07f4d4f1527d6aaa202dfd9", null ],
    [ "Delete", "class_project_1_1_logic_1_1_character_logic.html#ac65a7319327d13f5b6d8ceca6fbe3b50", null ],
    [ "GetAll", "class_project_1_1_logic_1_1_character_logic.html#a45c33c373557378fbf96800205f42984", null ],
    [ "GetById", "class_project_1_1_logic_1_1_character_logic.html#a0e521e50d08f3e16e005e92f8d264a79", null ],
    [ "SearchForSpecies", "class_project_1_1_logic_1_1_character_logic.html#a9cd496a534bbe7bbb44ce0714bbd8ed9", null ],
    [ "UpdateName", "class_project_1_1_logic_1_1_character_logic.html#ab4dc46f6d4883f6c0492de98f1f48d1a", null ]
];