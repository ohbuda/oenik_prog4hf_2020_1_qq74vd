var class_project_1_1_repository_1_1_battle_repository =
[
    [ "BattleRepository", "class_project_1_1_repository_1_1_battle_repository.html#ac52c00f42c085e3afdc2831a931df837", null ],
    [ "Create", "class_project_1_1_repository_1_1_battle_repository.html#a5dd2d3c982cd88a4ee0514ea8fe94d45", null ],
    [ "Delete", "class_project_1_1_repository_1_1_battle_repository.html#acaac749244756e2915dc9924cd07e2c6", null ],
    [ "GetAll", "class_project_1_1_repository_1_1_battle_repository.html#a083f5bd4abc9681a10c15694e4b0014e", null ],
    [ "GetById", "class_project_1_1_repository_1_1_battle_repository.html#a4e435623107370cfe4df14d4ee6c09cd", null ],
    [ "UpdateOutcome", "class_project_1_1_repository_1_1_battle_repository.html#aad86ee73edc33450204aa1cf78589e39", null ]
];