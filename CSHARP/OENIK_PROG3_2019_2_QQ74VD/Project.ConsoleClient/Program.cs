﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.ConsoleClient
{
    using System.Net.Http;

    using Newtonsoft.Json;

    public class Character
    {
        public int Id { get; set; }
        
        public string Name { get; set; }
        
        public DateTime Dob { get; set; }
        
        public DateTime Dod { get; set; }
        
        public string Species { get; set; }
        
        public string Gender { get; set; }

        public override string ToString()
        {
            return $"ID = {Id} \taName = {Name}\tDob = {Dob}\tDod={Dod}\tSpecies = {Species}\tGender = {Gender}";
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Waiting for input");
            Console.ReadLine();

            string url = "http://localhost:49984/api/CharactersAPI/";

            using (HttpClient client = new HttpClient())
            {
                string json = client.GetStringAsync(url + "all").Result;
                var list = JsonConvert.DeserializeObject<List<Character>>(json);
                foreach (var item in list)
                    Console.WriteLine(item);
                Console.ReadLine();

                Dictionary<string, string> postData;
                string response;

                postData = new Dictionary<string, string>();
                postData.Add(nameof(Character.Name), "Test");
                postData.Add(nameof(Character.Gender), "1");
                postData.Add(nameof(Character.Species), "fsd");
                postData.Add(nameof(Character.Dob), DateTime.MinValue.ToString());
                postData.Add(nameof(Character.Dod), DateTime.MaxValue.ToString());

                response = client.PostAsync(url + "add", new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;
                Console.WriteLine("ADD: "+response);
                json = client.GetStringAsync(url + "all").Result;
                Console.WriteLine("ALL: " +json);
                Console.ReadLine();

                int carId = JsonConvert.DeserializeObject<List<Character>>(json).Single(x => x.Name == "Test").Id;

                postData = new Dictionary<string, string>();
                postData.Add(nameof(Character.Id), carId.ToString());
                postData.Add(nameof(Character.Name), "Test");
                postData.Add(nameof(Character.Gender), "gsldjhgkdgh");
                postData.Add(nameof(Character.Species), "fsd");
                postData.Add(nameof(Character.Dob), DateTime.MinValue.ToString());
                postData.Add(nameof(Character.Dod), DateTime.MaxValue.ToString());

                response = client.PostAsync(url + "mod", new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;
                Console.WriteLine("MOD: " + response);
                Console.WriteLine("ALL: " + client.GetStringAsync(url + "all").Result);
                Console.ReadLine();

                Console.WriteLine("DEL: " + client.GetStringAsync(url+"del/"+carId).Result);
                Console.WriteLine("ALL: " + client.GetStringAsync(url + "all").Result);
                Console.ReadLine();


            }
        }
    }
}
    